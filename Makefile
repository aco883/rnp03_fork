all:	http-get echo-server1 echo-server2 echo-server3


breakdetector.o:	breakdetector.cc
	g++ breakdetector.cc -c -o breakdetector.o -O2 -g -Wall


http-get:	http-get.cc
	g++ http-get.cc -o http-get -O2 -Wall -g

echo-server1:	echo-server1.cc
	g++ echo-server1.cc -o echo-server1 -O2 -g -Wall

echo-server2:	echo-server2.cc breakdetector.o
	g++ echo-server2.cc breakdetector.o -o echo-server2 -O2 -g -Wall

echo-server3:	echo-server3.cc breakdetector.o
	g++ echo-server3.cc breakdetector.o -o echo-server3 -O3 -g -Wall -lpthread


clean:
	rm -f http-get echo-server1 echo-server2 echo-server3
